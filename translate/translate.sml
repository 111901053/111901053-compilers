structure Translate = struct

fun   tempToReg 0 = MIPS.t0
    | tempToReg 1 = MIPS.t1
    | tempToReg 2 = MIPS.t2
    | tempToReg 3 = MIPS.t3
    | tempToReg 4 = MIPS.t4
    | tempToReg 5 = MIPS.t5
    | tempToReg 6 = MIPS.t6
    | tempToReg 7 = MIPS.t7
    | tempToReg 8 = MIPS.s0
    | tempToReg 9 = MIPS.s1
    | tempToReg 10 = MIPS.s2
    | tempToReg 11 = MIPS.s3
    | tempToReg 12 = MIPS.s4
    | tempToReg 13 = MIPS.s5
    | tempToReg 14 = MIPS.s6
    | tempToReg 15 = MIPS.s7
    | tempToReg _ = MIPS.imm(1)


fun compileExpr env  (Ast.Const x) t = ([MIPS.INST(MIPS.LI(tempToReg(t) , MIPS.imm(x)))], env)
    | compileExpr env (Ast.Op(x1, Ast.Plus, x2)) t  =  let
                                                        val r1 = Temp.newtemp()
                                                        val r2 = Temp.newtemp()
                                                        val (leftres, newenv)       = compileExpr env x1 r1
                                                        val (rightres, new_newenv)  = compileExpr newenv x2 r2
                                                    in
                                                        (leftres@rightres@[MIPS.INST(MIPS.ADD(tempToReg(t), tempToReg(r1), tempToReg(r2)))], new_newenv)
                                                    end

    | compileExpr env (Ast.Op(x1, Ast.Minus, x2)) t  =  let
                                                        val r1 = Temp.newtemp()
                                                        val r2 = Temp.newtemp()
                                                        val (leftres, newenv)       = compileExpr env x1 r1
                                                        val (rightres, new_newenv)  = compileExpr newenv x2 r2
                                                    in
                                                        (leftres@rightres@[MIPS.INST(MIPS.SUB(tempToReg(t), tempToReg(r1), tempToReg(r2)))], new_newenv)
                                                    end

    | compileExpr env (Ast.Op(x1, Ast.Mul, x2)) t  =  let
                                                        val r1 = Temp.newtemp()
                                                        val r2 = Temp.newtemp()
                                                        val (leftres, newenv)       = compileExpr env x1 r1
                                                        val (rightres, new_newenv)  = compileExpr newenv x2 r2
                                                    in
                                                        (leftres@rightres@[MIPS.INST(MIPS.MUL(tempToReg(t), tempToReg(r1), tempToReg(r2)))], new_newenv)
                                                    end

    | compileExpr env (Ast.Var x) t              = let
                                                    val res =  case (AtomMap.find(env, Atom.atom(x))) of
                                                        (SOME (v)) =>  ([MIPS.INST(MIPS.MOVE(tempToReg(t), tempToReg(v)))], env)
                                                        |   NONE =>
                                                                let
                                                                val r1 = Temp.newtemp()
                                                                val newenv = AtomMap.insert(env, Atom.atom(x), r1)
                                                                in
                                                                    ([MIPS.INST(MIPS.MOVE(tempToReg(t), tempToReg(r1)))], newenv)
                                                                end
                                                in
                                                    res
                                                end




fun forhelper loop r1 r2 l1 l2 i j  = [
                                        MIPS.INST(MIPS.LI(tempToReg(r1), MIPS.imm(i))),
                                        MIPS.INST(MIPS.LI(tempToReg(r2), MIPS.imm(j))),
                                        MIPS.LABEL(MIPS.LUSER(Temp.labelToString(l1))),
                                        MIPS.INST(MIPS.BEQ(tempToReg(r1), tempToReg(r2), MIPS.LUSER(Temp.labelToString(l2))))
                                    ]
                                    @loop
                                    @[
                                        MIPS.INST(MIPS.ADDI(tempToReg(r1), tempToReg(r1), MIPS.imm(1))),
                                        MIPS.INST(MIPS.J(MIPS.LUSER(Temp.labelToString(l1)))),
                                        MIPS.LABEL(MIPS.LUSER(Temp.labelToString(l2)))
                                    ]




fun compileStmt env (Ast.Assign (x,y)) =
                        let
                            val res1 =  case (AtomMap.find(env, Atom.atom(x))) of
                            (SOME (v)) => let
                                val (ans, newenv) = compileExpr env y v
                                            in (ans, newenv) end
                            | NONE   =>

                            let
                                val newvar = Temp.newtemp()
                                val newenv = AtomMap.insert(env, Atom.atom(x), newvar)
                                val (res, new_newenv) = compileExpr newenv y newvar
                            in
                                (res, new_newenv)
                            end
                        in
                            res1
                        end

    | compileStmt env (Ast.Print x) =
                        let
                            val newvar = Temp.newtemp()
                            val (res, newenv) = compileExpr env x newvar
                            val new_newenv = AtomMap.insert(env, Atom.atom("a0"), newvar)

                        in
                            (res@[MIPS.INST(MIPS.MOVE(MIPS.a0, tempToReg(newvar)))]@[MIPS.INST(MIPS.LI(MIPS.v0, MIPS.imm(1)))]@[MIPS.INST(MIPS.SYSCALL)] , new_newenv)
                        end

    | compileStmt env (Ast.FOR (var, i, j, st)) =
                        let
                            val r1 = Temp.newtemp()
                            val r2 = Temp.newtemp()
                            val l1 = Temp.newlabel()
                            val l2 = Temp.newlabel()
                            val up = AtomMap.insert(env, Atom.atom(var), r1)
                            fun compileloop e [] = []
                                    | compileloop e (x :: xs) =
                                                                let
                                                                    val (res, new_env) = compileStmt e x
                                                                in
                                                                    res@compileloop new_env xs
                                                                end
                            val loop = compileloop up st
                        in
                            (forhelper loop r1 r2 l1 l2 i j, env )
                        end





fun compileprog env []           = []
    | compileprog env (x :: xs)   = let
                                    val (res, newenv) = compileStmt env x
                                in
                                    res@compileprog newenv xs
                                end

fun compile prog = [MIPS.DIR(MIPS.GLOBL("main")), MIPS.LABEL(MIPS.LUSER("main"))]@compileprog AtomMap.empty prog@[MIPS.INST(MIPS.LI(MIPS.v0, MIPS.imm(10)))]@[MIPS.INST(MIPS.SYSCALL)]

end
