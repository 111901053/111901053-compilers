signature TEMP =
  sig

     type label
     type temp
     val newlabel : unit -> label
     val newtemp  : unit -> temp

     val tempToString : temp -> string
     val labelToString : temp -> string

  end

structure Temp : TEMP = struct

   type temp  = int
   type label = int

   val nextTemp  = ref 0
   val nextLabel = ref 0

   fun newtemp _ = ( nextTemp := !nextTemp + 1; !nextTemp )

   fun tempToString t = "t" ^ Int.toString(t)

   fun newlabel _ = (nextLabel := !nextLabel + 1; !nextLabel)

   fun labelToString l = "l" ^ Int.toString(l)

end
