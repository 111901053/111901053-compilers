SUMMARY

* Lab1 
  
  * Added the functioning of *, / and () brackets to the reverse polish compiler. 
  * made changes in expr.lex and rp.lex for lexer, expr.grm for grammar and ast.sml for capturing. 

* Lab2 
  
  * Made the MIPS.sml file which functions to capture the AST of output. 
  * Defined functions for printing registers, directives and instructions fully manually. 
  * Defined functions to print the statement and program and label using previously defined functions. 
  * Defined datatypes like label, inst and reg. 
  
* Lab3
	
  * From this lab I started making the tiger compiler from the scratch. 
  * First made lex and grm files which do the lexing and parsing of the input. 
  * Made the ast.sml which captures the input AST. 
  * Directly transferred this AST into translate.sml which converts the input AST into output AST using mips.sml.
  * The intermediate registers used were done using temp.sml. 
  * The printing was done using ec.sml and mips.sml. 
  
* Lab4 
  
  * Created a way to capture for loops in the tiger compiler. 
  * Added labels to do jmp in assembly which is used for the working of for loops. 
  * Even nested for loops can be made using the same logic. 
  * The lexing and parsing, ast capture and translation of the keywords for, to, do, etc were also implemented.
  
* Lab5
  
  * Created basicblocks.sml to separate out the basic blocks created which will be together when the execution happens. 
  * Using sir's template and the algorithm sir gave in the lectures, implemented the way to take out basic blocks. 
  * All the problematic commands like beq, bge, etc which can break a basic block were separately taken care of. 
  
* Lab6
   * Made a way to implement graphs in compilers. 
   * In the sir's template, there were a lot of functions given like newnode, addedge, empty, etc each of which have their own functionality. I just completed the code for each function. This lab was separate from the other labs as nothing was in common from the previous ones. 
   
   
   
* Organistaion: 
 - The BasicBlocks folder contains the basicblocks.sml file which is implemented in lab5.  
 - The Graphs folder contains the GraphsForCompilers.sml file which is implemented in lab6. 
 - The ir folder contains the files ir.mlb and ir.sml which were used for implementing the IR structure, but in the end I didn't complete them and did the lab without IR. 
 - Lab0 contains stuff done for the demo lab like printing hello world, creating Changelog.md and readme.md
 - Makefile, this contains the commands to run executables. 
 - reverse-polish folder contains the reverse polish compiler given by sir. 
 - target folder contains the files mips.sml and temp.sml , which are kind of the files used to create the output/target and print the output AST as required. 
 - the test folder contains test cases for the for loops, given in lab4, and also their outputs.  
 - the tiger folder containst the initial stuff taken from the input, like the lex and grm file along with the ast.sml file. 
 - the translate folder contains the files ec.sml and translate.sml which were used to convert the input AST into Output AST. 
 - ec.mlb , which is not inside any folder as it is used to run a lot of files and its easier to put it outside. It is in the makefile and runs files. 
 
 
 
* TODO List 
 - I have to create the IR structure and implement it within the translate.sml where the intermediates are made using the IR structure. For now my intermediates are made directly using functions inside translate.sml. 
 - I have to do the lab7, which was to create the TREE structure. 
