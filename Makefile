
COMMON=tiger/ast.sml target/mips.sml target/temp.sml

%.lex.sml:%.lex
	mllex $<

%.grm.sml:%.grm
	mlyacc $<

all: ec

.PHONY: all clean test

clean:
	rm -f tiger/*.lex.sml
	rm -f tiger/*.grm.sml tiger/*.grm.desc tiger/*.grm.sig ec

ec: translate/ec.sml ec.mlb tiger/tiger.grm.sml tiger/tiger.lex.sml ${COMMON} translate/translate.sml
	mlton ec.mlb

test: all
	./ec < test/test.in > test/test.out
	spim -file test/test.out



# ${CURDIR}/ec test.in
