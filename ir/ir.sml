structure IR : sig

  type inst = (string, Temp.temp) MIPS.inst
  type stmt = (string, Temp.temp) MIPS.stmt
  type prog = stmt list
  val ppInst : inst -> string
  val ppStmt : stmt -> string
  val pp     : prog -> string

end = struct

  fun ppInst (MIPS.ADD (r1, r2, r3)) = "add " ^ Temp.tempToString r1 ^ ", " ^ Temp.tempToString r2 ^ ", " ^ Temp.tempToString r3
    | ppInst (MIPS.SUB (r1, r2, r3)) = "sub " ^ Temp.tempToString r1 ^ ", " ^ Temp.tempToString r2 ^ ", " ^ Temp.tempToString r3
    | ppInst (MIPS.MUL (r1, r2, r3)) = "mul " ^ Temp.tempToString r1 ^ ", " ^ Temp.tempToString r2 ^ ", " ^ Temp.tempToString r3

  fun ppStmt(x) = ppInst(x)

  fun pp [] = ""
    | pp (x :: xs) = (ppStmt x) ^ pp xs

    

end
