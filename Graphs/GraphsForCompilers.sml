exception KeyNotFound

signature GRAPH = sig

type node
type 'a graph

val empty   : unit -> 'a graph
val newNode : 'a graph -> 'a  -> node

val addEdge : 'a graph -> (node * node) -> unit

val succ    : 'a graph -> node -> node list
val pred    : 'a graph -> node -> node list
val label   : 'a graph -> node -> 'a

val clear   : 'a graph -> unit
val all     : 'a graph -> node list

end


functor MkGraph () :> GRAPH = struct

	type node = word

	structure NodeHashKey : HASH_KEY = struct
		type hash_key = node
		fun  hashVal w = w
		fun  sameKey (w1,w2) = w1 = w2
	end

	structure NodeSet = HashSetFn (NodeHashKey)

	type nodeSet = NodeSet.set

	type 'a graph =
    {
        labels : (node, 'a)  HashTable.hash_table,
		successors   : (node, nodeSet) HashTable.hash_table,
		predecessors : (node, nodeSet) HashTable.hash_table,
		nextNode : node ref
	}

	fun empty () =
    {
        labels = HashTable.mkTable (NodeHashKey.hashVal, NodeHashKey.sameKey) (0, KeyNotFound),
		successors = HashTable.mkTable (NodeHashKey.hashVal, NodeHashKey.sameKey) (0, KeyNotFound),
		predecessors = HashTable.mkTable (NodeHashKey.hashVal, NodeHashKey.sameKey) (0, KeyNotFound),
		nextNode   = ref (Word.fromInt 0)
    }

	val firstWord = (Word.fromInt 1)

	fun newNode (g : ('t graph)) a =  let
				val node = !(#nextNode g)
				val _ =	HashTable.insert (#labels g) (node, a)

				val first = NodeSet.mkEmpty 0
				val second = NodeSet.mkEmpty 0
				val _ = HashTable.insert (#successors g) (node, first)
				val _ = HashTable.insert (#predecessors g) (node, second)

				val _ = (#nextNode g) := node + firstWord
				in
					node
				end


	fun addEdge (g : ('t graph)) (x, y) = let val uset = HashTable.lookup (#successors g) x
											val _ = NodeSet.add (uset, y)

											val lset = HashTable.lookup (#predecessors g) y
											val _ = NodeSet.add (lset, x)

										in
											()
										end

	fun succ (g : ('t graph)) v = let val uset = HashTable.lookup (#successors g) v
										in
											NodeSet.listItems uset
										end

	fun pred (g : ('t graph)) v = let val lset = HashTable.lookup (#predecessors g) v
										in
											NodeSet.listItems lset
										end

	fun label (g : ('t graph)) v = HashTable.lookup (#labels g) v

	fun clear (g : ('t graph)) = let val _ = HashTable.clear (#labels g)
									val _ = HashTable.clear (#successors g)
									val _ = HashTable.clear (#predecessors g)
									val _ = (#nextNode g) = ref (Word.fromInt 0)
								in
									()
								end

	fun all (g : ('t graph)) = let val nodes = HashTable.listItemsi (#labels g)
										in
											map (fn n  => (#1 n)) nodes
										end

end
