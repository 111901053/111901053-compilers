signature INST = sig
	type t  (* The type of the instruction *)
	val isJumpLike 		: 	t -> bool
	val isTarget		: 	t -> bool
end

structure MIPSInst : INST = struct

	type t = (string, MIPS.reg) MIPS.stmt

	fun 	isJumpLike (MIPS.LABEL(_)) = false
		|	isJumpLike (MIPS.DIR(_)) = false
		| 	isJumpLike (MIPS.INST(x)) = case x of
			  MIPS.B    (_)						=> true
			| MIPS.BCZT (_)						=> true
			| MIPS.BCZF (_)						=> true
			| MIPS.BEQ  (_, _, _)				=> true
			| MIPS.BEQZ (_, _)					=> true
			| MIPS.BGE  (_, _, _)				=> true
			| MIPS.BGEU  (_, _, _)				=> true
			| MIPS.BGEZ (_, _)					=> true
			| MIPS.BGEZAL (_, _)				=> true
			| MIPS.BGT  (_, _, _)				=> true
			| MIPS.BGTZ (_, _)					=> true
			| MIPS.BLE  (_, _, _)				=> true
			| MIPS.BLEU  (_, _, _)				=> true
			| MIPS.BLEZ (_, _)					=> true
			| MIPS.BLTZAL (_, _)				=> true
			| MIPS.BLT  (_, _, _)				=> true
			| MIPS.BLTU  (_, _, _)				=> true
			| MIPS.BLTZ (_, _)					=> true
			| MIPS.BNE  (_, _, _)				=> true
			| MIPS.BNEZ (_, _)					=> true

		  (*Jump instructions*)
			| MIPS.J    (_)						=>	true
			| MIPS.JAL    (_)					=> 	true
			| MIPS.JALR   (_)					=> 	true
			| MIPS.JR    (_)					=> 	true
			| _									=> false


	fun isTarget (MIPS.LABEL(_))				= true
		| isTarget _							= false


end

functor BasicBlocks (I : INST) = struct

	structure Inst = I		 (* expose the instruction module as well *)

	type block = I.t list

	fun 	helper ([], a, res)			= 	res@[a]
		| 	helper (b :: bs, a, res)		= 	case (I.isJumpLike(b), I.isTarget(b)) of
														(true, false) 	=> helper (bs, [], res@[(a@[b])])
													|  	(false, false) 	=> helper (bs, a@[b],res)
													| 	(false, true)  	=> helper (bs, [b], res@[a])
													|	_				=> helper (bs, [], res@[a]@[[b]])

	fun basicblocks x = helper (x, [], [])

	fun printblocks [] = ""
		| printblocks (x :: xs) = (MIPS.prProg x) ^ "\n" ^ (printblocks xs)

end



structure MIPSBasicBlocks = BasicBlocks (MIPSInst)
