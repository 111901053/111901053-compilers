(* This is the preamble where you can have arbitrary sml code. For us
it is empty *)

%%
(* The terminals or tokens of the language *)
%term CONST of int
    | PLUS
    | MINUS
    | MUL
    | ASSIGN
    | EQUAL
    | PRINT
    | FOR
    | TO
    | DO
    | DONE
    | VAR of string
    | EOF
    | NEWLINE

(* The nonterminals of the language *)
%nonterm EXP     of Ast.Expr
       | EXPS    of Ast.Expr list
       | STMT    of Ast.Stmt
       | STMTS   of Ast.Stmt list
       | PROGRAM of Ast.Stmt list

%eop EOF (* What token to use as end of parsing token              *)
%verbose (* Generates a printed form of the table in expr.grm.desc *)
%pure


%name Expr (* The prefix to use on the name of LrValFun structure *)


%noshift EOF
(*

Operator precedence and associativity. The %left says the operator is
left associative and the precedence increase as you go down this list.

*)

%left PLUS MINUS  (* + and - are of same precedence *)
%left MUL         (* higher than + and -            *)

(* The type that captures position in the input *)
%pos int

%%

PROGRAM     : STMTS                ( STMTS )
            | STMTS NEWLINE        ( STMTS )

STMTS       : (* empty *)          ( []                )
            | STMT NEWLINE STMTS   ( STMT :: STMTS     )

EXP         : CONST                ( Ast.Const CONST )
            | VAR                  ( Ast.Var   VAR   )
            | EXP PLUS EXP         ( Ast.plus  EXP1 EXP2)
            | EXP MINUS EXP        ( Ast.minus EXP1 EXP2 )
            | EXP MUL   EXP        ( Ast.mul   EXP1 EXP2 )

STMT        : VAR ASSIGN EXP       ( Ast.assign VAR EXP)
            | PRINT EXP            ( Ast.Print EXP )
            | FOR VAR EQUAL CONST TO CONST NEWLINE DO NEWLINE STMTS DONE  (Ast.FOR (VAR,CONST1,CONST2, STMTS))
